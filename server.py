import asyncio
import logging
import grpc
import jwt

import transaction_pb2
import transaction_pb2_grpc
import orderservice_pb2
import orderservice_pb2_grpc

from log4mongo.handlers import BufferedMongoHandler

MONGODB_URL = "mongodb+srv://PIZZAKU:Hthg027n2uTubGZa@cluster0.rrcy1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority"

# Setup logging
logging.basicConfig(level=logging.INFO)
handler = BufferedMongoHandler(
    host=MONGODB_URL, database_name="logging-service", collection="logs"
)
logger = logging.getLogger("transaction-server")
logger.addHandler(handler)
logger.info("starting transaction grpc server")

# Coroutines to be invoked when the event loop is shutting down.
_cleanup_coroutines = []

TRANSACTION_GRPC_PORT = 50054

class Transaction(transaction_pb2_grpc.TransactionServicer):
    def transform_item_order(self, order_service_item_order):
        return transaction_pb2.ItemOrder(
            name=order_service_item_order.name,
            price=order_service_item_order.price,
            quantity=order_service_item_order.quantity
        )

    async def Pay(self, request, context):
        # JWT
        payload = jwt.decode(request.accessToken, options={"verify_signature": False})

        # Logging
        logger.info(
            f'Transaction request received, request: {payload["role"]} - {request.orderId} - {type(request)}'
        )

        if payload["role"] != "customer":
            logger.info("Role invalid")
            return transaction_pb2.PayReply(status=403, message="role is not customer")

        with grpc.insecure_channel("localhost:50053") as channel:
            stub = orderservice_pb2_grpc.OrderServiceStub(channel)
            response = stub.UpdateOrder(
                orderservice_pb2.UpdateOrderRequest(
                    accessToken=request.accessToken,
                    orderId=request.orderId,
                    orderStatus="pending_approval",
                )
            )
            status = response.status
            message = response.message
            order = response.order
            order_items = order.items

            transaction_service_order_items = [self.transform_item_order(item) for item in order_items]
            transaction_sevice_order = transaction_pb2.Order(id=order.id, orderStatus=order.orderStatus, items=transaction_service_order_items)
            logger.info(
            f'Changing orderId {request.orderId} status from "PENDING_PAYMENT" to "PENDING_APPROVAL"')
        return transaction_pb2.PayReply(status=status, message=message, order=transaction_sevice_order)


async def serve():
    server = grpc.aio.server()
    transaction_pb2_grpc.add_TransactionServicer_to_server(Transaction(), server)
    server.add_insecure_port(f"[::]:{TRANSACTION_GRPC_PORT}")
    await server.start()

    async def server_graceful_shutdown():
        logging.info("Starting graceful shutdown...")
        # Shuts down the server with 0 seconds of grace period. During the
        # grace period, the server won't accept new connections and allow
        # existing RPCs to continue within the grace period.
        await server.stop(5)

    _cleanup_coroutines.append(server_graceful_shutdown())
    await server.wait_for_termination()


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    try:
        loop.run_until_complete(serve())
    finally:
        loop.run_until_complete(*_cleanup_coroutines)
        loop.close()
